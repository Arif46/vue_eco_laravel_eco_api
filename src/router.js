import Vue from 'vue'
import VueRouter from 'vue-router'
import MainContent from '@/components/pages/MainContent'

Vue.use(VueRouter)

const routes = [
  
      {
          path:'/',
          name:'/MainContent',
          meta: {
            hideNavigation:true,
          },
          component:MainContent,
         
      },
      {
        path:'/category',
        name:'category',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/category/Category.vue')
      },
      {
        path:'/allcategory',
        name:'allcategory',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/category/AllCategory.vue')
      },
      {
        path:'/categoryedit/:id',
        name:'category.edit',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/category/EditCategory.vue')
      },
      {
        path:'/subcategory',
        name:'/subcategory',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/subcategory/Subcategory.vue')
      },
      {
        path:'/Allsubcategory',
        name:'/Allsubcategory',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/subcategory/AllSubcategory.vue')
      },
      {
        path:'/totalnumber',
        name:'/totalbumber',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/totalMark/totalmarks.vue')
      },
      {
        path:'/login',
        name:'login',
        meta: {
          hideNavigation:false,
        },
        component:()=>import('./components/Auth/Login.vue') 
      },
      {
        path:'/register',
        name:'register',
        meta: {
          hideNavigation:false,
        },
        component:()=>import('./components/Auth/Register.vue') 
      },
      {
        path:'/product',
        name:'product',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/product/Product.vue') 
      },
      {
        path:'/allproduct',
        name:'allproduct',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/product/AllProduct.vue') 
      },
      {
        path:'/multiplefile',
        name:'multiplefile',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/File/MultiFileinput.vue') 
      },
      {
        path:'/allmultiplefile',
        name:'allmultiplefile',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/File/Allinputfile.vue') 
      },
      {
        path:'/editsubcategory/:id',
        name:'subcategory.edit',
        meta: {
          hideNavigation:true,
        },
        component:()=>import('./components/subcategory/EditSubcategory.vue') 
      }

];

export const router = new VueRouter({
    mode: "history",
    routes
})

// router.beforeEach((to, from, next) => {
//   // redirect to login page if not logged in and trying to access a restricted page
//   const publicPages = ['/login'];
//   const authRequired = !publicPages.includes(to.path);
//   const loggedIn = localStorage.getItem('accessToken');

//   if (authRequired && !loggedIn) {
//       return next('login');
//   }

//   next();
// })